# Taller Flash N° 2
# Ejercicio 1 - Problema de cantAparicionesSub
# Autor: Gabriel Lichtenstein
# Fecha: 13/04/2016
#
# Usage: python3 cantApariciones.py
#
#~ Especifcar e implementar una funcion llamada cantAparicionesSub que 
#~ tome  como  parametro  dos  listas  de  enteros, y  que  calcule  la 
#~ cantidad  de elementos de la primera lista que se encuentran tambien 
#~ en la segunda.

lista1 = [1,2,3,4]
lista2 = [0,2,3,4,5,4] # notar que si hay duplicados, se cuentan cada vez que aparecen como un elemento distinto.

def cantAparicionesSub(lista1,lista2):
	contador = 0 #cuenta los elementos que hay en común en ambas listas.
	
	for i in range (0, len(lista1)):
		for j in range (0, len(lista2)):
			if (lista1[i] == lista2[j]):
				contador += 1
			
	return contador
	
# test
print(cantAparicionesSub(lista1,lista2))
