# Taller Flash N° 2
# Ejercicio 2 - Problema de lista "triangular"
# Autor: Gabriel Lichtenstein
# Fecha: 13/04/2016
#
# Usage: python3 listaTriangular.py
#
#~ Decimos que una lista es "triangular" si es creciente hasta cierto elemento, 
#~ y a partir de ese elemento es decreciente. 
#
#~ Por ejemplo, la lista [2;4;5;7;4;3] es triangular, 
#~ mientras que la lista [2;4;5;7;5;8;4;3;1] no lo es.
#
#~ Implementar una funcion llamada listaTriangular que tome como parametro 
#~ una lista de enteros, y que determine si la lista es triangular.


# ejemplos de listas triangulares
lista1 = [2,4,5,7,4,3] # True
lista2 = [2,4,5,7,5,8,4,3,1] # False
lista3 = [2,4,5,7,5,4,3,1,0] # True
lista4 = [2,4,5,7,8,9,10,9,8,4,3,1] # True
lista5 = [2,8,5,7,8,9,10,9,8,4,3,1] # False

lista = []
def listaTriangular(lista):
	
	i = 0 
	top = 0 
	
	# busca la punta/cima de la lista triangular:
	while (lista[i] < lista[i+1]): 
		top = lista[i+1] 
		i +=1
	
	# controla si los valores de la lista a partir de la cima
	# van en una secuencia decreciente hasta el final de la lista,
	# sino devuelve False.	
	while (i < (len(lista)-1)):
		
		if ( lista[i] > lista[i+1]):
			i +=1 		
		
		elif (lista[i] < lista[i+1]):
		    return False
	
	return True
			
# Prueba de ejemplos:
print(listaTriangular(lista1))
print(listaTriangular(lista2))
print(listaTriangular(lista3))
print(listaTriangular(lista4))
print(listaTriangular(lista5))
